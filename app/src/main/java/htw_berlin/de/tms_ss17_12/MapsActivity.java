package htw_berlin.de.tms_ss17_12;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import htw_berlin.de.tms_ss17_12.Daten.SQLite;
import htw_berlin.de.tms_ss17_12.Daten.Weinachtsmarkt;
import htw_berlin.de.tms_ss17_12.Daten.Weihnachtsmaerkte;
import htw_berlin.de.tms_ss17_21.R;

import static htw_berlin.de.tms_ss17_21.R.id.map;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    public static JSONObject rootObject;
    List<Weinachtsmarkt> alleMaerke = new ArrayList<>();
    private SQLite demoSqlHelper;
    private String m_Text = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(map);
        mapFragment.getMapAsync(this);

    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        demoSqlHelper = new SQLite(this);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        getJSONStuff();
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(52.5170365, 13.3888599), 9.5f));
        setMarkers();
    }

    private void setMarkers() {
        for (Weinachtsmarkt wm : alleMaerke) {
            String latitude = wm.getLATITUDE();
            String longitude = wm.getLONGITUDE();
            String id = wm.getID();
            String name = wm.getNAME();

            latitude = latitude.replace(",", ".");
            longitude = longitude.replace(",", ".");
            // Setting Markers on Map!
            Marker marker = mMap.addMarker(
                    new MarkerOptions()
                            .position(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)))
                            .title(name)
            );
            marker.setTag(Integer.parseInt(id));

            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {
                    mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                        @Override
                        public void onInfoWindowClick(Marker marker) {
                            System.out.print("click");
                        }
                    });

                    return null;
                }
            });
        }
    }

    private void getJSONStuff() {

        //  Getting the JSON stuff..
        //try {
        Weihnachtsmaerkte abc = new Weihnachtsmaerkte();
        //  Use GJSON for getting the geo data to visualize the positions in maps fragment
        try {
            alleMaerke = abc.getall();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void savePostion(final View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Title");
        final EditText input = new EditText(this);
        builder.setView(input);
        final Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Location myloc = mMap.getMyLocation();
                Address aa = null;
                try {
                    aa = geocoder.getFromLocation(myloc.getLatitude(), myloc.getLongitude(), 1).get(0);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                demoSqlHelper.insert(myloc.getLatitude(), myloc.getLongitude(), input.getText().toString());

                Toast.makeText(v.getContext(), "Standort gespeichert " + aa.getAddressLine(0), Toast.LENGTH_SHORT).show();
                m_Text = input.getText().toString();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.setNegativeButton("Delete all", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SQLiteDatabase db = null;
                demoSqlHelper.clearTable( db );
            }
        });

        builder.show();
    }

    public void ladeOrte(View v) {
        ArrayList<SQLite.Saved_location> selection = demoSqlHelper.get_all();
        for (int i = 0; i < selection.size(); i++) {
            SQLite.Saved_location tmp = selection.get(i);
            double lat_v = (double) tmp.lat;
            double lng_v = (double) tmp.lng;
            System.out.println(lat_v + " " + lng_v);
            BitmapDescriptor subwayBitmapDescriptor = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW);
            mMap.addMarker(
                    new MarkerOptions()
                            .position(new LatLng(lat_v, lng_v))
                            .icon(subwayBitmapDescriptor)
                            .title(tmp.titel)
            );
        }
    }
}


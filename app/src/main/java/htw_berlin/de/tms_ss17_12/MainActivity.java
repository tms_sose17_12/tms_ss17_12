package htw_berlin.de.tms_ss17_12;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import htw_berlin.de.tms_ss17_12.Daten.Weinachtsmarkt;
import htw_berlin.de.tms_ss17_12.Daten.WeinachtsmarktFragment;
import htw_berlin.de.tms_ss17_12.Daten.Weihnachtsmaerkte;
import htw_berlin.de.tms_ss17_21.R;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_ACCESS_FINE_LOCATION = 1;
    private static final int REQUEST_INTERNET = 2;
    public static Weinachtsmarkt currentMarket = null;
    public static Weihnachtsmaerkte wheihnachtsmaerkte;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String gifName = "name.gif";

        String yourData = "<html style=\"margin: 0;\">\n" +
                "    <body style=\"margin: 0;\">\n" +

               "    <img src=" + "https://media.giphy.com/media/Z5rm4BuSbMLAs/giphy.gif" + " style=\"width: 100%; height: 100%\" />\n" +
                "    </body>\n" +
                "    </html>";
        WebView web = (WebView) findViewById(R.id.webv);
        web.loadData(yourData, "text/html; charset=utf-8", "UTF-8");


        ImageView image = (ImageView) findViewById(R.id.imageView);

        image.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent();
                intent.setAction(android.content.Intent.ACTION_VIEW);

                Uri imageUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
                        "://" + getResources().getResourcePackageName(R.drawable.fahr_plan)
                        + '/' + getResources().getResourceTypeName(R.drawable.fahr_plan) + '/' + getResources().getResourceEntryName(R.drawable.fahr_plan) );

                Bitmap bm = BitmapFactory.decodeResource( getResources(), R.drawable.fahr_plan);

                String extStorageDirectory = Environment.getDataDirectory().toString();
                File file = new File(extStorageDirectory, "MyIMG.png");
                FileOutputStream outStream = null;
                try {
                    outStream = new FileOutputStream(file);
                    bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
                    outStream.flush();
                    outStream.close();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Uri imguri=Uri.fromFile(file);
                intent.setDataAndType(Uri.parse(imguri.getPath()), "image/png");
                startActivity(intent);
            }
        });

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    public void fragmentclick(View v) {

        WeinachtsmarktFragment l = (WeinachtsmarktFragment)getSupportFragmentManager().findFragmentByTag("weinachtsmarktFragment");
        if(l == null) {
            l = new WeinachtsmarktFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(android.R.id.content, l, "weinachtsmarktFragment");
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    public void alle_suchen(View v) {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
        checkPermissions();
    }
    private void checkPermissions(){
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_ACCESS_FINE_LOCATION);
            }
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, REQUEST_INTERNET);
            }
        }
    }

    public static ArrayList<Weinachtsmarkt> get_weihnachtsmaerkte() {
        ArrayList<Weinachtsmarkt> alle_weinachtsmarkt = new ArrayList<Weinachtsmarkt>();
        wheihnachtsmaerkte = new Weihnachtsmaerkte();
        try {
            alle_weinachtsmarkt  = (ArrayList<Weinachtsmarkt>) wheihnachtsmaerkte.getall();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  alle_weinachtsmarkt;
    }

    public void about(MenuItem item){
        Toast.makeText(this, getString(R.string.MadeBy), Toast.LENGTH_LONG).show();
        final TextView input = new TextView(this);
        AlertDialog ad;
        ad = new AlertDialog.Builder(this).create();
        ad.setMessage(getString(R.string.contact));
        ad.show();
        ad.setView(input);
    }



}

package htw_berlin.de.tms_ss17_12.Daten;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import htw_berlin.de.tms_ss17_12.MainActivity;
import htw_berlin.de.tms_ss17_21.R;


public class WeinachtsmarktFragment extends ListFragment {
    ArrayList<String> wm_name = new ArrayList<>();
    ArrayList<Weinachtsmarkt> list = new ArrayList<>();

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewGroup rootview = (ViewGroup) inflater.inflate(R.layout.fragment_weinachtsmarkt_list, container, false);
        try {
             list = (ArrayList<Weinachtsmarkt>) MainActivity.get_weihnachtsmaerkte();

            Location myloc = getLocation();
            System.out.println(myloc.getLatitude() + " " + myloc.getLongitude());
            for (Weinachtsmarkt w : list) {
                Location wm = new Location("1");
                double wm_lat = Double.parseDouble(w.getLATITUDE().replace(",", "."));
                double wm_lng = Double.parseDouble(w.getLONGITUDE().replace(",", "."));
                wm.setLatitude(wm_lat);
                wm.setLongitude(wm_lng);
                DecimalFormat df = new DecimalFormat("#.##");
                double distance = Double.parseDouble(df.format(myloc.distanceTo(wm) / 1000));
                String info = w.getNAME() + ": " + w.getSTRASSE() + " ( " + distance + " KM )";
                System.out.println(info.substring(info.length() - 10).replace("(", "").replace(" ", "").replace(")", "").replace("KM", "") + "  = " + distance);
                wm_name.add(info);
            }
            Collections.sort(wm_name, new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    double wm_a = Double.parseDouble(o1.substring(o1.length() - 10).replace("(", "").replace(" ", "").replace(")", "").replace("KM", ""));
                    double wm_b = Double.parseDouble(o2.substring(o2.length() - 10).replace("(", "").replace(" ", "").replace(")", "").replace("KM", ""));
                    return Double.compare(wm_a, wm_b);
                }
            });
            ArrayAdapter<String> adapter = new ArrayAdapter(getActivity(), R.layout.fragment_weinachtsmarkt, R.id.wm_name, wm_name);
            setListAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rootview;
    }

    public Location getLocation() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null;
        }
        LocationManager mLocationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {

            Location l = mLocationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long rowId) {
        ViewGroup viewGroup = (ViewGroup) v;
        TextView txt = (TextView) viewGroup.findViewById((R.id.wm_name));
        String name = txt.getText().toString();
        String lat = null;
        String lng = null;
        for (Weinachtsmarkt wm : list) {
            if (wm.getNAME().contains(name.substring(0, 10))) {
                lat = wm.getLATITUDE();
                lng = wm.getLONGITUDE();
                lat = lat.replace(",",".");
                lng = lng.replace(",",".");
            }
        }
        String l_uri = "google.navigation:q=" + lat + "," + lng;
        // Do the onItemClick action
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(l_uri));
        startActivity(intent);
        Toast.makeText(getContext(), "Viel Spaß in " + name, Toast.LENGTH_SHORT).show();
    }

}

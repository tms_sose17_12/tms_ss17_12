package htw_berlin.de.tms_ss17_12.Daten;

/**
 * Created by Me on 18.06.2017.
 */

public class Weinachtsmarkt {

    private String ID = "id";
    private String BEZIRK = "bezirk";
    private String NAME = "name";
    private String STRASSE = "strasse";
    private String PLZ_ORT = "plz_ort";
    private String VERANSTALTER = "veranstalter";
    private String VON = "von";
    private String BIS = "bis";
    private String OEFFNUNGSZEITEN = "oeffnungszeiten";
    private String EMAIL = "email";
    private String W3 = "w3";
    private String BEMERKUNGEN = "bemerkungen";
    private String LATITUDE = "lat";
    private String LONGITUDE = "lng";

    public void setID(String ID) {
        this.ID = ID;
    }

    public void setBEZIRK(String BEZIRK) {
        this.BEZIRK = BEZIRK;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public void setSTRASSE(String STRASSE) {
        this.STRASSE = STRASSE;
    }

    public void setPLZ_ORT(String PLZ_ORT) {
        this.PLZ_ORT = PLZ_ORT;
    }

    public void setVERANSTALTER(String VERANSTALTER) {
        this.VERANSTALTER = VERANSTALTER;
    }

    public void setVON(String VON) {
        this.VON = VON;
    }

    public void setBIS(String BIS) {
        this.BIS = BIS;
    }

    public void setOEFFNUNGSZEITEN(String OEFFNUNGSZEITEN) {
        this.OEFFNUNGSZEITEN = OEFFNUNGSZEITEN;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public void setW3(String w3) {
        W3 = w3;
    }

    public void setBEMERKUNGEN(String BEMERKUNGEN) {
        this.BEMERKUNGEN = BEMERKUNGEN;
    }

    public void setLATITUDE(String LATITUDE) {
        this.LATITUDE = LATITUDE;
    }

    public void setLONGITUDE(String LONGITUDE) {
        this.LONGITUDE = LONGITUDE;
    }


    public String getID() {
        return ID;
    }

    public String getBEZIRK() {
        return BEZIRK;
    }

    public String getNAME() {
        return NAME;
    }

    public String getSTRASSE() {
        return STRASSE;
    }

    public String getPLZ_ORT() {
        return PLZ_ORT;
    }

    public String getVERANSTALTER() {
        return VERANSTALTER;
    }

    public String getVON() {
        return VON;
    }

    public String getBIS() {
        return BIS;
    }

    public String getOEFFNUNGSZEITEN() {
        return OEFFNUNGSZEITEN;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public String getW3() {
        return W3;
    }

    public String getBEMERKUNGEN() {
        return BEMERKUNGEN;
    }

    public String getLATITUDE() {
        return LATITUDE;
    }

    public String getLONGITUDE() {
        return LONGITUDE;
    }
}

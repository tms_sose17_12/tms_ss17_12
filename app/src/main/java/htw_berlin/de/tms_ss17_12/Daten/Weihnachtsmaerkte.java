package htw_berlin.de.tms_ss17_12.Daten;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;


/**
 * Created by Me on 06.07.2017.
 */

public class Weihnachtsmaerkte {
    String URL_WEIHNACHTSMAERKTE = "http://www.berlin.de/sen/wirtschaft/service/maerkte-feste/weihnachtsmaerkte/index.php/index/all.gjson?q=";
    private List<Weinachtsmarkt> allemaerkte = new ArrayList<>();



    public Weihnachtsmaerkte() {

    }

    public List<Weinachtsmarkt> getall() throws MalformedURLException, ExecutionException, InterruptedException, JSONException {


        JSONGetter abc = new JSONGetter();
        abc.execute(new URL(URL_WEIHNACHTSMAERKTE));
        String result = abc.get();


        JSONObject rootObject = new JSONObject(result);
        JSONArray featuresArray = rootObject.getJSONArray("features");

        HashMap<String, String>[] data = new HashMap[featuresArray.length()];
        for (int i = 0; i < featuresArray.length(); i++) {
            try {
                JSONObject containerObject = featuresArray.getJSONObject(i);
                JSONObject propertiesObject = containerObject.getJSONObject("properties");
                JSONObject dataObject = propertiesObject.getJSONObject("data");

                Weinachtsmarkt tmp = new Weinachtsmarkt();
                tmp.setID(dataObject.getString(tmp.getID()));
                tmp.setNAME(dataObject.getString("name"));
                tmp.setLATITUDE(dataObject.getString("lat"));
                tmp.setLONGITUDE(dataObject.getString("lng"));
                tmp.setBEMERKUNGEN(dataObject.getString(tmp.getBEMERKUNGEN()));
                tmp.setBEZIRK(dataObject.getString(tmp.getBEZIRK()));
                tmp.setSTRASSE(dataObject.getString(tmp.getSTRASSE()));
                allemaerkte.add(tmp);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return allemaerkte;
    }

}
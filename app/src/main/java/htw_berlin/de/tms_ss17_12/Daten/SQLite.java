package htw_berlin.de.tms_ss17_12.Daten;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class SQLite extends SQLiteOpenHelper {

	private static final String TAG = SQLite.class.getSimpleName();

	private static final String DATABASE_NAME = "demo.db";
	private static final int DATABASE_VERSION = 1;
	
	private static final String FIRSTTABLE_NAME = "firsttable";
	private static final String LAT_COLUMN = "lat";
	private static final String LNG_COLUMN = "lng";
	private static final String TITEL_COLUMN = "titel";

	public class Saved_location {
		public double lat;
		public double lng;
		public String titel;

	}
	
	//SQL
	private static final String CREATE_FIRSTTABLE =
			"CREATE TABLE " + FIRSTTABLE_NAME + 
			" (" + LAT_COLUMN + " INTEGER," + LNG_COLUMN  + " TEXT," + TITEL_COLUMN + " TEXT)";
	
	private static final String DROP_FIRSTTABLE =
			"DROP TABLE IF EXISTS " + FIRSTTABLE_NAME; 
	
	public SQLite(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_FIRSTTABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.d(TAG, "Upgrade from version: " + oldVersion + " to " + newVersion);
		clearTable(db);
	}

	public void insert( double lat, double lng, String Titel) {
		long row = -1;
		
		try {
			SQLiteDatabase db = getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put(LAT_COLUMN, lat);
			values.put(LNG_COLUMN, lng);
			values.put(TITEL_COLUMN, Titel);
			row = db.insert(FIRSTTABLE_NAME, null, values);
		} catch ( SQLiteException e) {
			Log.e(TAG, "insert error 1",e);
		} catch (Exception e) {
			Log.e(TAG, "insert error 2",e);
		} finally {
			Log.d(TAG, "inserted: " + row);
		}
	}
	
	public String query() {
		SQLiteDatabase db = getWritableDatabase();
		String q = "SELECT * FROM " + FIRSTTABLE_NAME;
		Cursor mCursor = db.rawQuery(q, null);
		StringBuilder sb = new StringBuilder();
		String name;
		String age;
		while (mCursor.moveToNext()) {
			name = mCursor.getString(mCursor.getColumnIndex(LAT_COLUMN));
			age  = mCursor.getString(mCursor.getColumnIndex(LNG_COLUMN));
            //("\"" + age + "\"  \"" + name +"\"\n" );
            sb.append("\"");
            sb.append(age);
            sb.append("\"  \"");
            sb.append(name);
            sb.append("\"\n");
		}

		mCursor.close();
		return sb.toString();
	}


	public ArrayList<Saved_location> get_all() {
		ArrayList<Saved_location> result = new ArrayList<Saved_location>();
		SQLiteDatabase db = getWritableDatabase();
		String q = "SELECT * FROM " + FIRSTTABLE_NAME;
		Cursor mCursor = db.rawQuery(q, null);

		double LAT;
		double LNG;
		while (mCursor.moveToNext()) {
			Saved_location tmp = new Saved_location();
			tmp.lat = mCursor.getDouble(mCursor.getColumnIndex(LAT_COLUMN));//getString(mCursor.getColumnIndex(LAT_COLUMN));
			tmp.lng  = mCursor.getDouble(mCursor.getColumnIndex(LNG_COLUMN));//getString(mCursor.getColumnIndex(LNG_COLUMN));
			tmp.titel = mCursor.getString(mCursor.getColumnIndex(TITEL_COLUMN));
			//("\"" + age + "\"  \"" + name +"\"\n" );
			result.add(tmp);
		}

		mCursor.close();
		return result;
	}

	public void clearTable(SQLiteDatabase db) {
		db =  getWritableDatabase();
		   db.execSQL(DROP_FIRSTTABLE);
		db.execSQL(CREATE_FIRSTTABLE);
	}
}


